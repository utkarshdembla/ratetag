package org.utility;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

public class Utility {

	public double getrandomPrice(String currency)
	{
		Random random=new Random();
		double min=500000;
		double max=3000000;
		
		double price=random.nextDouble()*(max-min)+min;
		
		if(currency.equalsIgnoreCase("INR"))
		{
			price=round(price,2);
			return price;
		}
		else if(currency.equalsIgnoreCase("Dollar") || currency.equalsIgnoreCase("Dollars"))
		{
			price=price/60;
			price=round(price,2);
			return price;
		}
		else
		{
			return 0;
		}
	}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
}

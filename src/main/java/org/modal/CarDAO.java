package org.modal;


public class CarDAO{

	private long id;
	private String brand;
	private String model;
	private double price;
	

	public CarDAO()
	{
		
	}
	
	public CarDAO(long id,String brand,String model)
	{
		this.id=id;
		this.brand=brand;
		this.model=model;
		
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "[id=" + id + ", brand=" + brand + ", model=" + model + ", price=" + price + "]";
	}        
	
	
	
		
	
	
}

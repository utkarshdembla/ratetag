package org.resource;

import org.modal.CarDAO;
import org.service.Service;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Resource {

	Service service = new Service();
	CarDAO carDAO = new CarDAO();

	@RequestMapping("/cars")
	public ModelAndView getInfo(@RequestParam(value = "brand", required = true) String brand,
			@RequestParam(value = "model", required = true) String model,
			@RequestParam(value = "currency", required = false) String currency) {

		ModelAndView mv = new ModelAndView();
		mv.setViewName("display.jsp");
		if (model !="" && brand !="") {
			if (currency.equalsIgnoreCase("INR") || currency.equalsIgnoreCase("Dollar")
					|| currency.equalsIgnoreCase("Dollars")) {
				carDAO = service.getCarInfo(brand, model, currency);
				if (carDAO != null) {
					mv.addObject("output", carDAO.toString());
				}

				else {
					mv.addObject("output", "Mismatch-Please choose the CarModel and CarBrand correctly");
				}

			} else if (currency == "") {
				mv.addObject("output", "Please provide INR or Dollars as currency");
			} else {
				mv.addObject("output", "Product price is not available in " + currency);
			}
		}

		else {
			mv.addObject("output", "Please fill the details for Brand and Model");
		}

		return mv;

	}

	@RequestMapping("/feature")
	public ModelAndView getFeature(@RequestParam(value = "model", required = true) String model) {
		ModelAndView mv = new ModelAndView();
		if (model == null) {
			mv.addObject("output", "Please add the model to get its feature");
		} else {
			mv.addObject("output", service.getCarFeature(model));
		}

		mv.setViewName("display.jsp");
		return mv;
	}

}

package org.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.database.Database;
import org.factory.Car;
import org.factory.FordFactory;
import org.modal.CarDAO;
import org.utility.Utility;

public class Service {

	private Map<Long, CarDAO> map = Database.getAllCars();

	public Service() {
		map.put(1L, new CarDAO(1, "Fiat", "Punto"));
		map.put(2L, new CarDAO(2, "Fiat", "Linea"));
		map.put(3L, new CarDAO(3, "Ford", "Ecosport"));
		map.put(4L, new CarDAO(4, "Ford", "Figo"));
		map.put(5L, new CarDAO(5, "Toyota", "Etios"));
		map.put(6L, new CarDAO(6, "Toyota", "Fortuner"));
		map.put(7L, new CarDAO(7, "Toyota", "Altis"));

	}

	public CarDAO getCarInfo(String brand, String model, String currency) {
		CarDAO carDAO = new CarDAO();

		for(Entry<Long,CarDAO> entry : map.entrySet())
		{
			if(entry.getValue().getBrand().equalsIgnoreCase(brand) && entry.getValue().getModel().equalsIgnoreCase(model))
			{
				carDAO.setBrand(brand);
				carDAO.setModel(model);
				carDAO.setId(entry.getValue().getId());
				Utility util=new Utility();
				carDAO.setPrice(util.getrandomPrice(currency));
				
				return carDAO;
			}
		}
		return null;
		
		
	}

	public String getCarFeature(String model) {

		FordFactory fordFactory = new FordFactory();
		String feature = "";
		if (model.equalsIgnoreCase("Figo") || model.equalsIgnoreCase("Ecosport")
				|| model.equalsIgnoreCase("Endeavour")) {
			Car car = fordFactory.getCarFeatures(model);
			feature = car.feature();
		}
		else
		{
			feature="Please enter the  models as - Figo,Ecosport,Endeavour";
		}

		return feature;
	}

}

package org.factory;



public class FordFactory {
	
	public Car getCarFeatures(String carName)
	{
		if(carName==null)
		{
			return null;
		}
		
		if(carName.equalsIgnoreCase("Figo"))
		{
			return new Figo();
		}
		else if(carName.equalsIgnoreCase("Endeavour"))
		{
			return new Endeavour();
		}
		
		else if(carName.equalsIgnoreCase("Ecosport"))
		{
			return new Ecosport();
		}
		
		return null;
	}
	
}
